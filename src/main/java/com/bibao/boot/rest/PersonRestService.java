package com.bibao.boot.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bibao.boot.dao.PersonDao;
import com.bibao.boot.model.Person;

@RestController
@RequestMapping("/person")
public class PersonRestService {
	@Autowired
	private PersonDao personDao;
	
	//@RequestMapping(value="/ping", method=RequestMethod.GET)
	@GetMapping("/ping")
	public String ping() {
		return "Ping service is working properly";
	}
	
	@GetMapping
	public List<Person> findAllPerson() {
		return personDao.findAll();
	}
	
	@GetMapping("/{id}")
	public Person findPersonById(@PathVariable("id") int id) {
		Person person = personDao.findById(id);
		return person;
	}
	
	@PostMapping
	public Person savePerson(@RequestBody Person person) {
		Person savedPerson = personDao.save(person);
		return savedPerson;
	}
	
	@PutMapping
	public Person updatePerson(@RequestBody Person person) {
		Person updatedPerson = personDao.update(person);
		return updatedPerson;
	}
	
	@DeleteMapping("/{id}")
	public void deletePersonById(@PathVariable("id") int id) {
		personDao.deleteById(id);
	}
}
