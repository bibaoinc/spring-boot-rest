package com.bibao.boot.rest;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import com.bibao.boot.model.Person;
import com.bibao.boot.springbootrest.SpringBootRestApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootRestApplication.class)
public class PersonRestServiceIntegrationTest {
	@Autowired
	private PersonRestService service;
	
	@Test
	public void testFindPersonById() {
		Person person = service.findPersonById(2);
		assertEquals(2, person.getId());
		assertEquals("Bob", person.getFirstName());
		assertEquals("Zhu", person.getLastName());
	}

	@Test
	public void testFindAllPerson() {
		List<Person> personList = service.findAllPerson();
		assertFalse(CollectionUtils.isEmpty(personList));
	}
	
	@Test
	public void testSavePerson() {
		int sizeBeforeSave = service.findAllPerson().size();
		Person person = new Person();
		person.setFirstName("Mary");
		person.setLastName("Zhao");
		Person savedPerson = service.savePerson(person);
		int sizeAfterSave = service.findAllPerson().size();
		assertEquals(sizeBeforeSave + 1, sizeAfterSave);
		assertEquals(4, savedPerson.getId());
		assertEquals("Mary", savedPerson.getFirstName());
		assertEquals("Zhao", savedPerson.getLastName());
	}
}
