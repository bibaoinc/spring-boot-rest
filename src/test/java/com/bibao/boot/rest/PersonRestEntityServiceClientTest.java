package com.bibao.boot.rest;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.bibao.boot.model.Person;

public class PersonRestEntityServiceClientTest {
	private static final String BASE_URI = "http://localhost:8080/spring-boot-rest/rest/entity";
	private static final String ID_URI = BASE_URI + "/{id}";
	private RestTemplate template;
	
	@Before
	public void setUp() throws Exception {
		template = new RestTemplate();
	}

	@Test
	public void testFindPersonById() {
		ResponseEntity<Person> response = template.getForEntity(ID_URI, Person.class, "1");
		assertEquals(HttpStatus.OK, response.getStatusCode());
		Person person = response.getBody();
		assertEquals(1, person.getId());
		assertEquals("Alice", person.getFirstName());
		assertEquals("Liu", person.getLastName());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testFindAllPerson() {
		ResponseEntity<?> response = template.getForEntity(BASE_URI, List.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		List<Person> personList = (List<Person>)response.getBody();
		assertFalse(CollectionUtils.isEmpty(personList));
	}
}
